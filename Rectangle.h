#pragma once
#include "Shader.h"

class Rectangle
{
public:
	void renderScene(const Shader& shader);
	void renderCube();
	unsigned int CreateTexture(const std::string& strTexturePath);

private:
	unsigned int cubeVAO = 0;
	unsigned int cubeVBO = 0;

};

