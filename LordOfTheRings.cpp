﻿#include <glad/glad.h>
#include <glfw3.h>

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <iostream>

#include "Globals.h"
#include "Shader.h"
#include "Model.h"
#include "Camera.h"

#pragma comment (lib, "glfw3dll.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "OpenGL32.lib")

// settings
const unsigned int SCR_WIDTH = 1920;
const unsigned int SCR_HEIGHT = 1080;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow* window);

void prepareShader(const unsigned int textureID, Shader& shader, glm::mat4& model);

// camera
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;
float nearPlane = 1.0f, farPlane = 48.0f;
bool moveLight = true;
bool moveRing = false;

unsigned int skyboxVAO, skyboxVBO;
//functions
unsigned int LoadCubemap(std::vector<std::string> faces);
void RenderFloor();
void RenderScene(const Shader& shader);
unsigned int CreateTexture(const std::string& strTexturePath);
//functions for skybox
float* GenerateSkybox();
std::vector<std::string> GenerateFacePath();
void DrawSkybox(Shader& skyboxShader, glm::mat4& view, glm::mat4& projection, GLuint& cubemapTexture);

GLFWwindow* initializeWindow();

int main(int argc, char** argv)
{
	std::string strFullExeFileName = argv[0];
	std::string strExePath;
	const size_t last_slash_idx = strFullExeFileName.rfind('\\');
	if (std::string::npos != last_slash_idx) {
		strExePath = strFullExeFileName.substr(0, last_slash_idx);
	}

	GLFWwindow* window = initializeWindow();

	//shader declaration
	Shader shadowMappingShader("ShadowMapping.vs", "ShadowMapping.fs");
	Shader shadowMappingDepthShader("ShadowMappingDepth.vs", "ShadowMappingDepth.fs");
	shadowMappingDepthShader.Use();

	unsigned int texture = CreateTexture(strExePath + "\\Stones.jpg");
	unsigned int textureCube = CreateTexture(strExePath + "\\Black.jpg");

	// configure depth map FBO
	const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);
	// create depth texture
	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// shader configuration
	shadowMappingShader.Use();
	shadowMappingShader.SetInt("diffuseTexture", 0);
	shadowMappingShader.SetInt("shadowMap", 1);

	Shader modelShader("lighting.vs", "lighting.fs");
	glm::vec3 lightPos(10.0f, 40.75f, 0.0f);
	modelShader.SetInt("texture_diffuse1", 0);
	glm::vec3 lightColors(70.25f, 70.25f, 70.25f);
	Shader skyboxShader("skybox.vertexshader", "skybox.fragmentshader");

	//Skybox skybox;
	float* skyboxVertices = GenerateSkybox();
	// Cubemap (Skybox)
	std::vector<std::string> facesPath = GenerateFacePath();
	GLuint cubemapTexture = LoadCubemap(facesPath);

	glEnable(GL_CULL_FACE);

	//models
	Model firstCharacter("legolas/LegolasFixed.obj");
	Model witchking("witchking/WitchKing.obj");
	Model gollum("gollum2/lowPoly5.obj");
	Model shield("shield/MESH_GG_Shield2.obj");
	Model armor("armor/base-char-male.obj");
	Model ring("ring2/ring.obj");
	Model sword("sword/anduril.obj");
	Model warrior("warrior/warrior final textures.obj");
	Model horse("horseee/Cheval WSH.obj");
	float angle = 0.0f;
	// render loop
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// input
		processInput(window);

		if (moveLight == true)
		{
			lightPos.x = 2 * cos(currentFrame);
			lightPos.z = 2 * sin(currentFrame);
		}

		// render
		glClearColor(0.1f, 0.1f, 0.1f, 0.1f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//enable shader before setting uniforms
		glm::mat4 lightProjection, lightView;
		glm::mat4 lightSpaceMatrix;
		lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, nearPlane, farPlane);
		lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
		lightSpaceMatrix = lightProjection * lightView;

		//render scene from light's point of view
		shadowMappingDepthShader.Use();
		shadowMappingDepthShader.SetMat4("lightSpaceMatrix", lightSpaceMatrix);

		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		RenderScene(shadowMappingDepthShader);
		glCullFace(GL_BACK);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glDisable(GL_CULL_FACE);

		// reset viewport
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//render scene as normal using the generated depth/shadow map 
		glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		shadowMappingShader.Use();
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 500.0f);
		glm::mat4 view = camera.GetViewMatrix();
		shadowMappingShader.SetMat4("projection", projection);
		shadowMappingShader.SetMat4("view", view);

		// set light uniforms
		shadowMappingShader.SetVec3("viewPos", camera.Position);
		shadowMappingShader.SetVec3("lightPos", lightPos);
		shadowMappingShader.SetMat4("lightSpaceMatrix", lightSpaceMatrix);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textureCube);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		RenderScene(shadowMappingShader);
		glDisable(GL_CULL_FACE);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthMap);
		RenderScene(shadowMappingShader);
		glDisable(GL_CULL_FACE);

		glm::mat4 model2 = glm::mat4(1.0f);
		model2 = glm::translate(model2, glm::vec3(0.0f, 2.15f, 0.0f));
		model2 = glm::scale(model2, glm::vec3(7.2f, 7.0f, 7.0f));
		shadowMappingShader.SetMat4("model", model2);
		RenderFloor();

		glm::mat4 modelArmor = glm::mat4(1.0f);
		modelArmor = glm::translate(modelArmor, glm::vec3(-135.0f, 10.0f, -135.0));
		modelArmor = glm::scale(modelArmor, glm::vec3(20.0f, 20.0f, 20.0f));
		modelArmor = glm::rotate(modelArmor, glm::radians(0.0f), glm::vec3(1.f, 0.f, 0.f));
		shadowMappingShader.SetMat4("model", modelArmor);
		armor.Draw(modelShader);

		glm::mat4 modelLegolas = glm::mat4(1.0f);
		modelLegolas = glm::translate(modelLegolas, glm::vec3(135.0f, 10.0f, -135.0f));
		modelLegolas = glm::scale(modelLegolas, glm::vec3(0.3f, 0.3f, 0.3f));
		shadowMappingShader.SetMat4("model", modelLegolas);
		firstCharacter.Draw(modelShader);

		glm::mat4 modelWitchKing = glm::mat4(1.0f);
		modelWitchKing = glm::translate(modelWitchKing, glm::vec3(135.0f, 10.0f, 135.0f));
		modelWitchKing = glm::scale(modelWitchKing, glm::vec3(0.5f, 0.5f, 0.5f));
		modelWitchKing = glm::rotate(modelWitchKing, glm::radians(-90.0f), glm::vec3(1.f, 0.f, 0.f));
		modelWitchKing = glm::rotate(modelWitchKing, glm::radians(-180.0f), glm::vec3(0.f, 0.f, 1.f));
		shadowMappingShader.SetMat4("model", modelWitchKing);
		witchking.Draw(modelShader);

		glm::mat4 modelGollum = glm::mat4(1.0f);
		modelGollum = glm::translate(modelGollum, glm::vec3(0.0f, 20.0f, -129.0f));
		modelGollum = glm::scale(modelGollum, glm::vec3(20.0f, 20.0f, 20.0f));
		shadowMappingShader.SetMat4("model", modelGollum);
		gollum.Draw(modelShader);

		glm::mat4 modelShield = glm::mat4(1.0f);
		modelShield = glm::translate(modelShield, glm::vec3(0.0f, 35.0f, 0.0f));
		modelShield = glm::scale(modelShield, glm::vec3(0.5f, 0.5f, 0.5f));
		modelShield = glm::rotate(modelShield, glm::radians(180.0f), glm::vec3(1.f, 0.f, 0.f));
		shadowMappingShader.SetMat4("model", modelShield);
		shield.Draw(modelShader);

		glm::mat4 modelSword = glm::mat4(1.0f);
		modelSword = glm::translate(modelSword, glm::vec3(-125.0f, 30.0f, 10.0f));
		modelSword = glm::scale(modelSword, glm::vec3(5.5f, 5.5f, 5.5f));
		modelSword = glm::rotate(modelSword, glm::radians(90.0f), glm::vec3(1.f, 0.f, 1.f));
		modelSword = glm::rotate(modelSword, glm::radians(45.0f), glm::vec3(1.f, 0.f, 0.f));
		shadowMappingShader.SetMat4("model", modelSword);
		sword.Draw(modelShader);

		glm::mat4 modelRing = glm::mat4(1.0f);
		modelRing = glm::translate(modelRing, glm::vec3(0.0f, 17.0f, 135.0f));
		if (moveRing == true) {
			angle += 1.0f;
			modelRing = glm::rotate(modelRing, glm::radians(angle), glm::vec3(0.f, 1.f, 0.f));
		}
		modelRing = glm::scale(modelRing, glm::vec3(5.5f, 5.5f, 5.5f));

		modelRing = glm::rotate(modelRing, glm::radians(0.0f), glm::vec3(1.f, 0.f, 0.f));
		shadowMappingShader.SetMat4("model", modelRing);
		ring.Draw(modelShader);

		glm::mat4 modelWarrior = glm::mat4(1.0f);
		modelWarrior = glm::translate(modelWarrior, glm::vec3(-135.0f, 12.0f, 135.0));
		modelWarrior = glm::scale(modelWarrior, glm::vec3(15.0f, 15.0f, 15.0f));
		modelWarrior = glm::rotate(modelWarrior, glm::radians(0.0f), glm::vec3(1.f, 0.f, 0.f));
		shadowMappingShader.SetMat4("model", modelWarrior);
		warrior.Draw(modelShader);

		glm::mat4 modelHorse = glm::mat4(1.0f);
		modelHorse = glm::translate(modelHorse, glm::vec3(140.0f, 30.0f, 7.0f));
		modelHorse = glm::scale(modelHorse, glm::vec3(1.2f, 1.2f, 1.2f));
		modelHorse = glm::rotate(modelHorse, glm::radians(30.0f), glm::vec3(0.f, 0.f, 1.f));
		modelHorse = glm::rotate(modelHorse, glm::radians(10.0f), glm::vec3(0.f, 1.f, 0.f));
		modelHorse = glm::rotate(modelHorse, glm::radians(-60.0f), glm::vec3(1.f, 0.f, 0.f));

		shadowMappingShader.SetMat4("model", modelHorse);
		horse.Draw(modelShader);

		//Draw skybox as last
		DrawSkybox(skyboxShader, view, projection, cubemapTexture);

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// glfw: terminate, clearing all previously allocated GLFW resources.
	glfwTerminate();
	system("pause");
	return 0;
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS)
		camera.ProcessKeyboard(UP, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
		camera.ProcessKeyboard(DOWN, deltaTime);

	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
	{
		nearPlane += 0.5f;
		std::cout << farPlane << " far \n";
		std::cout << nearPlane << " near\n";
	}
	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
	{
		nearPlane -= 0.5f;
		std::cout << farPlane << " far \n";
		std::cout << nearPlane << " near\n";
	}
	if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
	{
		farPlane += 0.5f;
		std::cout << farPlane << " far \n";
		std::cout << nearPlane << " near\n";
	}
	if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
	{
		farPlane -= 0.5f;
		std::cout << farPlane << " far \n";
		std::cout << nearPlane << " near\n";
	}
	//not shadow
	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		farPlane = 0.1f;
		nearPlane = 0.1f;
	}
	//shadow
	if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
	{
		farPlane = 48.0f;
		nearPlane = 1.0f;
	}
	//rotate ring
	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
		moveRing = true;
	//stop rotate ring
	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS)
		moveRing = false;
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

unsigned int planeVAO = 0;
void RenderFloor()
{
	unsigned int planeVBO;

	if (planeVAO == 0) {
		// set up vertex data (and buffer(s)) and configure vertex attributes
		float dimension = 250.f;
		float planeVertices[] = {
			// positions            // normals         // texcoords
			 dimension, -0.5f,  dimension,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-dimension, -0.5f,  dimension,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
			-dimension, -0.5f, -dimension,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

			 dimension, -0.5f,  dimension,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-dimension, -0.5f, -dimension,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
			 dimension, -0.5f, -dimension,  0.0f, 1.0f, 0.0f,  25.0f, 25.0f
		};
		// plane VAO
		glGenVertexArrays(1, &planeVAO);
		glGenBuffers(1, &planeVBO);
		glBindVertexArray(planeVAO);
		glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindVertexArray(0);
	}
	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}

unsigned int cubeVAO = 0;
unsigned int cubeVBO = 0;
void renderCube()
{
	// initialize (if necessary)
	if (cubeVAO == 0)
	{
		float vertices[] = {
			// back face
			-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
			1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
			// front face
			-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
			1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
			1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
			1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
			-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
			-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
			// left face
			-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
			-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
			-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
			-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
			-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
			-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
			// right face
			1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
			1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
			1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
			1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
			1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
			1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
			// bottom face
			-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
			1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
			1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
			1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
			-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
			-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
			// top face
			-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
			1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
			1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
			1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
			-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
			-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
		};
		glGenVertexArrays(1, &cubeVAO);
		glGenBuffers(1, &cubeVBO);
		// fill buffer
		glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
		// link vertex attributes
		glBindVertexArray(cubeVAO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	// render Cube
	glBindVertexArray(cubeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

// renders the 3D scene
void RenderScene(const Shader& shader)
{
	glm::mat4 model;

	// cube
	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-135.0f, 5.0f, 135.0));//pus
	model = glm::scale(model, glm::vec3(10.0f, 5.0f, 7.0f));
	shader.SetMat4("model", model);
	renderCube();

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(135.0f, 5.0f, 135.0));//pus
	model = glm::scale(model, glm::vec3(10.0f, 5.0f, 7.0f));
	shader.SetMat4("model", model);
	renderCube();

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(135.0f, 5.0f, -135.0));//pus
	model = glm::scale(model, glm::vec3(10.0f, 5.0f, 7.0f));
	shader.SetMat4("model", model);
	renderCube();

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-135.0f, 5.0f, -135.0));//pus
	model = glm::scale(model, glm::vec3(10.0f, 5.0f, 7.0f));
	shader.SetMat4("model", model);
	renderCube();

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(135.0f, 5.0f, 0.0));//pus
	model = glm::scale(model, glm::vec3(10.0f, 5.0f, 7.0f));
	shader.SetMat4("model", model);
	renderCube();

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(-135.0f, 5.0f, 0.0));//pus
	model = glm::scale(model, glm::vec3(10.0f, 5.0f, 7.0f));
	shader.SetMat4("model", model);
	renderCube();

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, 5.0f, 0.0));
	model = glm::scale(model, glm::vec3(10.0f, 5.0f, 7.0f));
	shader.SetMat4("model", model);
	renderCube();

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, 5.0f, -135.0));
	model = glm::scale(model, glm::vec3(10.0f, 5.0f, 7.0f));
	shader.SetMat4("model", model);
	renderCube();

	model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0f, 5.0f, 135.0));
	model = glm::scale(model, glm::vec3(10.0f, 5.0f, 7.0f));
	shader.SetMat4("model", model);
	renderCube();
}

unsigned int CreateTexture(const std::string& strTexturePath)
{
	unsigned int textureId = -1;

	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true);
	unsigned char* data = stbi_load(strTexturePath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else {
		std::cout << "Failed to load texture: " << strTexturePath << std::endl;
	}
	stbi_image_free(data);

	return textureId;
}

unsigned int LoadCubemap(std::vector<std::string> faces)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		unsigned char* data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
			);
			stbi_image_free(data);
		}
		else
		{
			std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
			stbi_image_free(data);
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	return textureID;
}

float* GenerateSkybox()
{

	float dimension = 300.f;
	//skybox
	float skyboxVertices[] = {
		-dimension,  dimension, -dimension,
		-dimension, -dimension, -dimension,
		 dimension, -dimension, -dimension,
		 dimension, -dimension, -dimension,
		 dimension,  dimension, -dimension,
		-dimension,  dimension, -dimension,

		-dimension, -dimension,  dimension,
		-dimension, -dimension, -dimension,
		-dimension,  dimension, -dimension,
		-dimension,  dimension, -dimension,
		-dimension,  dimension,  dimension,
		-dimension, -dimension,  dimension,

		dimension, -dimension, -dimension,
		dimension, -dimension,  dimension,
		dimension,  dimension,  dimension,
		dimension,  dimension,  dimension,
		dimension,  dimension, -dimension,
		dimension, -dimension, -dimension,

		-dimension, -dimension,  dimension,
		-dimension,  dimension,  dimension,
		 dimension,  dimension,  dimension,
		 dimension,  dimension,  dimension,
		 dimension, -dimension,  dimension,
		-dimension, -dimension,  dimension,

		-dimension,  dimension, -dimension,
		 dimension,  dimension, -dimension,
		 dimension,  dimension,  dimension,
		 dimension,  dimension,  dimension,
		-dimension,  dimension,  dimension,
		-dimension,  dimension, -dimension,

		-dimension, -dimension, -dimension,
		-dimension, -dimension,  dimension,
		 dimension, -dimension, -dimension,
		 dimension, -dimension, -dimension,
		-dimension, -dimension,  dimension,
		 dimension, -dimension,  dimension
	};

	// Setup skybox VAO

	glGenVertexArrays(1, &skyboxVAO);
	glGenBuffers(1, &skyboxVBO);
	glBindVertexArray(skyboxVAO);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glBindVertexArray(0);

	return skyboxVertices;
}

GLFWwindow* initializeWindow()
{
	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Lord Of The Rings", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return NULL;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// glad: load all OpenGL function pointers
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return NULL;
	}
	// configure global opengl state
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// configure global opengl state
	glEnable(GL_DEPTH_TEST);

	return window;
}

void DrawSkybox(Shader& skyboxShader, glm::mat4& view, glm::mat4& projection, GLuint& cubemapTexture)
{
	glDepthFunc(GL_LEQUAL);  // Change depth function so depth test passes when values are equal to depth buffer's content
	skyboxShader.Use();
	view = glm::mat4(glm::mat3(camera.GetViewMatrix()));	// Remove any translation component of the view matrix

	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.GetID(), "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.GetID(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));

	//skybox cube
	glBindVertexArray(skyboxVAO);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTexture);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	glDepthFunc(GL_LESS); // Set depth function back to default
}

std::vector<std::string> GenerateFacePath()
{
	std::vector<std::string> facesPath
	{
		"Resources/skybox3_nx.jpg",//right
		"Resources/skybox3_px.jpg",//left
		"Resources/skybox3_py.jpg",//up
		"Resources/skybox3_ny.jpg",//bottom
		"Resources/skybox3_pz.jpg",//front
		"Resources/skybox3_nz.jpg"//back
	};

	return facesPath;
}

void setCurrentShader(Shader& shader)
{
	shader.Use();
	glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 500.0f);
	glm::mat4 view = camera.GetViewMatrix();

	shader.SetMat4("projection", projection);
	shader.SetMat4("view", view);
}

void setCurrentTexture(const unsigned int& textureID)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glDisable(GL_CULL_FACE);
}

void prepareShader(const unsigned int textureID, Shader& shader, glm::mat4& model)
{
	auto resetModel = [](glm::mat4& model) { model = glm::mat4(); };

	setCurrentShader(shader);
	setCurrentTexture(textureID);
	resetModel(model);
}