#pragma once
#include "Camera.h"

__declspec(selectany) Camera camera(glm::vec3(-10.0f, 30.0f, 240.0f));

// timing
__declspec(selectany) double deltaTime = 0.0f;    // time between current frame and last frame
__declspec(selectany) double lastFrame = 0.0f;